package lab9.event;

/**
* A class representing an event and its properties
*/
public class Event
{
    /** Name of event */
    private String name;
    
    // TODO: Make instance variables for representing beginning and end time of event
    
    // TODO: Make instance variable for cost per hour
    
    // TODO: Create constructor for Event class
    
    /**
    * Accessor for name field. 
    * @return name of this event instance
    */
    public String getName()
    {
        return this.name;
    }
    
    // TODO: Implement toString()
    
    // HINT: Implement a method to test if this event overlaps with another event
    //       (e.g. boolean overlapsWith(Event other)). This may (or may not) help
    //       with other parts of the implementation.
}
